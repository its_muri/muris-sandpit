import settings
import open3d as o3d
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import KDTree
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('ply_file')
args = parser.parse_args()

grid_size = settings.grid_size
distance_thresh = settings.distance_thresh

print("Reading Point Cloud ...")
pcl = o3d.io.read_point_cloud(args.ply_file)
cl, ind = pcl.remove_statistical_outlier(nb_neighbors=20,
                                                    std_ratio=2.0)
pcl = pcl.select_down_sample(ind)
pts = np.asarray(pcl.points)

X, Y = np.mgrid[np.amin(pts[:,0]):np.amax(pts[:,0])+grid_size:grid_size, \
    np.amin(pts[:,1]):np.amax(pts[:,1])+grid_size:grid_size]

xy = np.vstack((X.flatten(), Y.flatten())).T
q_pcl = KDTree(xy)

print("Making grid ...")
grid = {}
for i in pts:
    dst, ind = q_pcl.query(i[:2].reshape(1,2),1)
    if ind[0][0] in grid.keys():
        grid[ind[0][0]].append(i[2])
    else:
        grid[ind[0][0]] = [i[2]]

grid_means = {}
for i in grid.keys():
    grid_means[i] = np.mean(np.array(grid[i]))

def pick_points(pcd):
    vis = o3d.visualization.VisualizerWithEditing()
    vis.create_window()
    vis.add_geometry(pcd)
    vis.run()  # user picks points
    vis.destroy_window()
    return vis.get_picked_points()


if settings.select_thresh_manually:
    pts2 = pick_points(pcl)
    distance_thresh = np.asarray(pcl.points)[pts2][0][2]

print("Picking valid locations ...")
valids = []
for i in grid_means:
    if grid_means[i] < distance_thresh:
        valids.append(i)

if settings.visualize_plot:
    plt.scatter(pts[:,0], pts[:,1])
    plt.scatter(xy[:,0], xy[:,1])
    plt.scatter(xy[:,0][valids], xy[:,1][valids])
    plt.show()


ans = np.zeros((X.shape[1],X.shape[0]), dtype=bool)
for i in valids:
    ans[X.shape[1]-i%X.shape[1]][i//X.shape[1]] = 1

if settings.invert_thresh_direction:
    np.savetxt("ans.csv", ans, delimiter=",")
else:
    np.savetxt("ans.csv", 1-ans, delimiter=",")